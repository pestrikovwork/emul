using System.IO;
using System.Text;

namespace rkkpackageemul.Models
{
    public class StreamBinaryReader
    {
        public static string Read(Stream stream)
        {
            string resp = "";
            // This method reads in the file-format specific values.
            using (BinaryReader reader = new BinaryReader(stream))
            {
                int count = reader.ReadInt32();
                for (int i = 0; i < count; i++)
                {
                    string u = reader.ReadString();
                    int len = reader.ReadInt32();
                    byte[] b = reader.ReadBytes(len);
                    resp += Encoding.UTF8.GetString(b);
                }
            }
            
            return resp;
        }
    }
}