using System;
using Microsoft.AspNetCore.Mvc;
using System.Security.Cryptography.Pkcs;
using System.Security.Cryptography.X509Certificates;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Text;
using System.IO.Compression;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;

namespace PackageRequest.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class NbchController : ControllerBase
    {
        AppOptions appOptions;
        public IConfiguration Configuration;

        public NbchController(IOptions<AppOptions> options)
        {
            appOptions = options.Value;
        }

        [HttpGet]
        [Route("{id}")]
        public FileContentResult NbchGet(string id)
        {
            string path = appOptions.LogingPath;

            string[] files = Directory.GetFiles(appOptions.RKK_NbchResponcePath);

            if (appOptions.Loging)
            {
                System.IO.File.AppendAllText(System.IO.Path.Combine(path, "log.txt"), DateTime.Now.ToString() + "NBCH DEBUG    " + id + "\n");
            }

            byte[] arrayRead = new byte[0];
            FileContentResult fileContent = new FileContentResult(arrayRead, "multipart/form-data");

            try
            {
                foreach (string fileName in files)
                {
                    if (fileName.Contains(id.Remove(id.IndexOf(".") - 1)))
                    {
                        if (appOptions.Loging)
                        {
                            System.IO.File.AppendAllText(System.IO.Path.Combine(path, "log.txt"), DateTime.Now.ToString() + "NBCH DEBUG    " + fileName + "\n");
                            System.IO.File.AppendAllText(System.IO.Path.Combine(path, "log.txt"), DateTime.Now.ToString() + "NBCH DEBUG    " + id.Remove(id.IndexOf(".") - 1) + "\n");
                        }

                        using (FileStream fstream = System.IO.File.OpenRead(fileName))
                        {
                            arrayRead = new byte[fstream.Length];
                            fstream.Read(arrayRead, 0, arrayRead.Length);
                        }

                        fileContent = new FileContentResult(arrayRead, "multipart/form-data");

                        if (appOptions.Loging)
                        {
                            System.IO.File.AppendAllText(System.IO.Path.Combine(path, "log.txt"), DateTime.Now.ToString() + "NBCH DEBUG -->> Response file read in array Success\n");
                        }

                        //System.IO.File.Delete(fileName);
                        return fileContent;
                    }
                }
            }
            catch (Exception ex)
            {
                System.IO.File.AppendAllText(System.IO.Path.Combine(path, "log.txt"), DateTime.Now.ToString() + ex + "\n"); 
            }

             if (appOptions.Loging)
            {
                System.IO.File.AppendAllText(System.IO.Path.Combine(path, "log.txt"), DateTime.Now.ToString() + "NBCH DEBUG -->> Response file read in array FAIL\n");
            }

            return fileContent;
        }

        [HttpPost]
        [RequestSizeLimit(209_715_200)]
        public async void NbchPost()
        {
            //string id = DateTime.Now.ToString("yyyyMMddHHmmss") + "_" + Guid.NewGuid().ToString();
            string path = appOptions.LogingPath + "NBCH\\";
            string xmlData = "";

            if (Request.Body != null)
            {
                var streamReader = new StreamReader(Request.Body);
                xmlData = await streamReader.ReadToEndAsync();
            }

            /* if (true)
            {
                string path = @"C:\log\RKK";
                DirectoryInfo directoryInfo = new DirectoryInfo(path);
                if (!directoryInfo.Exists)
                {
                    directoryInfo.Create();
                }

                using (FileStream stream = new FileStream($"{path}\\" + id + "_request.xml", FileMode.OpenOrCreate))
                {
                    byte[] array = Encoding.Default.GetBytes(xmlData);
                    stream.Write(array, 0, array.Length);
                }
            } */

            try
            {
                //var file = Request.Form.Files[0];
                //string fileName = file.Name;
                string fileName = xmlData.Substring(xmlData.IndexOf("6801BB"), xmlData.IndexOf(".XML.gz") - xmlData.IndexOf("6801BB"));
                if (appOptions.Loging)
                {
                    System.IO.File.AppendAllText(System.IO.Path.Combine(path, "log.txt"), DateTime.Now.ToString() + "NBCH DEBUG    " + fileName + "\n");
                }

                string[] files = Directory.GetFiles(appOptions.RKK_NbchResponcePath);
                
                //DateTime date = DateTime.Now;
                //string stringDate = date.ToString("yyyyMMdd") + "_" + date.ToString("hhmmss") + "_Reply.XML.gz.p7s.p7m";
                //string responseFileName = fileName.Substring(fileName.IndexOf(".") + 1, fileName.Length - fileName.IndexOf(".") - 1) + "_Reply.XML.gz.p7s.p7m";
                string responseFileName = fileName + "_Reply.XML.gz.p7s.p7m";
                System.IO.File.AppendAllText(System.IO.Path.Combine(path, "log.txt"), DateTime.Now.ToString() + "NBCH DEBUG    " + responseFileName + "\n");

                //System.IO.File.Move(files[0], files[0].Remove(files[0].IndexOf('.') + 1) + stringDate);
                try
                {
                    System.IO.File.AppendAllText(System.IO.Path.Combine(path, "log.txt"), DateTime.Now.ToString() + "NBCH DEBUG    " + files[0] + "\n");
                    System.IO.File.Move(files[0], files[0].Remove(files[0].IndexOf('6')) + responseFileName);
                }
                catch (Exception ex)
                {
                    System.IO.File.AppendAllText(System.IO.Path.Combine(path, "log.txt"), DateTime.Now.ToString() + "NBCH DEBUG    " + ex + "\n");
                }
                
                // записываем пришедший файл на винт
                /*using (var fileStream = new FileStream(path + fileName, FileMode.Create))
                {
                    file.CopyTo(fileStream);
                }*/
            }
            catch (Exception e)
            {
                await Response.WriteAsync(e.Message);
            }
        }
    }
}