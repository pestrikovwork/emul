using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using Microsoft.Net.Http.Headers;

namespace PackageRequest.Controllers
{
    [Route("[controller]")]
    public class ExperianController : Controller
    {
        private readonly IConfiguration Configuration;
        private bool _logginEnabled;

        public ExperianController(IConfiguration configuration)
        {
            Configuration = configuration;
            _logginEnabled = bool.Parse(Configuration["LogIncomming"]);
        }

        [HttpPost]
        [RequestSizeLimit(209_715_200)]
        public async Task<IActionResult> OkbList()
        {
            DateTime startEmul = DateTime.Now;
            string id = DateTime.Now.ToString("yyyyMMddHHmmss") + "_" + Guid.NewGuid().ToString();

            string xmlData = "";
            if (Request.Body != null)
            {
                var streamReader = new StreamReader(Request.Body);
                xmlData = await streamReader.ReadToEndAsync();
            }

            if (bool.Parse(Configuration["LogIncomming"]))
            {
                string path = Configuration["LogingPath"] + "Experian";
                DirectoryInfo directoryInfo = new DirectoryInfo(path);
                if (!directoryInfo.Exists)
                {
                    directoryInfo.Create();
                }

                using (FileStream stream = new FileStream($"{path}\\" + id + "_request.xml", FileMode.OpenOrCreate))
                {
                    byte[] array = Encoding.Default.GetBytes(xmlData);
                    stream.Write(array, 0, array.Length);
                }
            }

            string trimText = xmlData.Substring(xmlData.LastIndexOf("Content-Disposition: form-data; name=\"ActionFlag\"") + 51);
            string actionFlag = trimText.Substring(0, trimText.IndexOf("--"));
            string resp = "";
            if (Int32.Parse(actionFlag.Trim()) == 7)
            {
                DateTime date = DateTime.Now;

                resp = "<s>\n" +
                    "<s n=\"Data\">\n" +
                    "<a n=\"ActionFlag\">7</a>\n" +
                    "<c n=\"History\">\n" +
                    "<s>\n" +
                    "<s n=\"warnings\">\n" +
                    "<a n=\"present\">0</a>\n" +
                    "</s>\n" +
                    "<c n=\"wlRecords\">\n" + 
                    "</c>\n" +
                    "<a n=\"wlReturnCount\">0</a>\n" +
                    "</s>\n" +
                    "</c>\n" +
                    "<a n=\"StreamID\">30564169</a>\n" +
                    "<a n=\"ValidationErrors\" />\n" +
                    "<a n=\"errorCode\">0</a>\n" +
                    "<a n=\"responseDate\">" + date.ToString("yyyyMMddhhmmss") + "</a>\n" +
                    "<a n=\"supportMail\" />\n" +
                    "</s>\n" +
                    "</s>\n";
            }
            else if (Int32.Parse(actionFlag.Trim()) == 9)
            {
                string[] files = Directory.GetFiles(Configuration["RKK_EiResponcePath"]); //Берем список файлов для ответа отсюда

                List<string> nameFiles = new List<string>();
                foreach (string s in files)
                {
                    string[] words = s.Split(new char[] { '\\' });
                    nameFiles.Add(words[4]);
                }

                nameFiles.ToArray();

                string str = "<s><a n = \"Name\">???</a></s>";
                string strXml = "";
                for (int i = 0; i < nameFiles.Count; i++)
                {
                    nameFiles[i] = str.Replace("???", nameFiles[i]);
                    strXml += nameFiles[i].ToString() + "\n";
                }

                DateTime date = DateTime.Now;

                resp = "<s>\n" +
                                  "<s n=\"Data\">\n" +
                                      "<a n=\"ActionFlag\">9</a>\n" +
                                      "<c n=\"Files\">\n" +
                                      "<s>\n" +
                                      "<c n= \"wlListOfFiles\">\n" +
                                      "???" +
                                      "</c>\n" +
                                      "<a n=\"wlReturnCount\">" + nameFiles.Count().ToString() + "</a>\n" +
                                      "</s>\n" +
                                      "</c>\n" +
                                      "<a n=\"StreamID\">30564169</a>\n" +
                                      "<a n=\"ValidationErrors\"/>\n" +
                                      "<a n=\"errorCode\">0</a>\n" +
                                      "<a n=\"responseDate\">" + date.ToString("yyyyMMddhhmmss") + "</a>\n" +
                                  "</s>\n" +
                              "</s>\n";

                resp = resp.Replace("???", strXml);
            }
            else if (Int32.Parse(actionFlag.Trim()) == 1)
            {
                Regex regex = new Regex("RESP(.*?)pem.pem");
                MatchCollection matches = regex.Matches(xmlData);
                string[] files = Directory.GetFiles(Configuration["RKK_EiResponcePath"]); //Берем список файлов для ответа отсюда

                foreach (Match match in matches)
                {
                    foreach (string s in files)
                    {
                        if (s.IndexOf(match.Value) > 0)
                        {
                            var fstream = System.IO.File.OpenRead(Configuration["RKK_EiResponcePath"] + match.Value);
                
                            if (_logginEnabled)
                                await LogRequestAsync(startEmul);

                            await SleepAsync();

                            return EndRequest(fstream, match.Value);
                        }
                    }
                }
            }
            else
            {
                DateTime date = DateTime.Now;

                resp = "<s>\n" +
                    "<c n=\"ValidationErrors\">\n" +
                    "<s>\n" +
                    "<a n=\"number\">503 Service Unavailable</a>\n" +
                    "</s>\n" +
                    "</c>\n" +
                    "<a n=\"errorCode\">1</a>\n" +
                    "<a n=\"responseDate\">" + date.ToString("yyyyMMddhhmmss") + "</a>\n" +
                    "<a n=\"streamID\">30564169</a>\n" +
                    "</s>\n";

            }
            if (_logginEnabled)
            {
                string path =Configuration["LogingPath"] + "Experian"; //пишем лог ответа сюда
                DirectoryInfo directoryInfo = new DirectoryInfo(path);
                if (!directoryInfo.Exists)
                {
                    directoryInfo.Create();
                }

                using (FileStream stream = new FileStream($"{path}\\" + id + "_response.xml", FileMode.OpenOrCreate))
                {
                    byte[] array = Encoding.Default.GetBytes(resp);
                    stream.Write(array, 0, array.Length);
                }
            }


            if (_logginEnabled)
                await LogRequestAsync(startEmul);

            return EndRequest(resp);
        }

        public Task LogRequestAsync(DateTime startEmul)
        {
            DateTime dateTime = DateTime.Now;
            using (StreamWriter writer = System.IO.File.AppendText(Configuration["LogingPath"] + "Experian\\OKB.csv")) //пишем лог ответа сюда
                return writer.WriteLineAsync(dateTime.ToString("yyyy.MM.dd HH:mm:ss,fff") + ";" + dateTime.Subtract(startEmul).TotalMilliseconds.ToString());
        }

        public Task SleepAsync()
        {
            return Task.Delay(int.Parse(Configuration["SleepExperian"]));
        }

        public IActionResult EndRequest(Stream stream, string Filename)
        {
            Response.Headers.Add("Content-Disposition",String.Format("attachment; Filename={0}; Filename*=UTF-8''{0}", Filename));

            return File(stream, "application/xml");
        }

        public IActionResult EndRequest(string xml)
        {
            var bytes = UTF8Encoding.UTF8.GetBytes(xml);

            return File(new MemoryStream(bytes), "application/xml");
        }
    }
}