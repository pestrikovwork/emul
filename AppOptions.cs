namespace PackageRequest
{
    public class AppOptions
    {
        public bool UseCrypto { get; set; } 
        public string Thumprint { get; set; }
        public bool LogIncomming { get; set; }
        public bool Loging { get; set; }
        public string LogingPath { get; set; }
        public int SleepNbch { get; set; }
        public int SleepExperian { get; set; }
        public int SleepEquifax { get; set; }
        public string FtpAddressIn { get; set; }
        public string FtpAddressOut { get; set; }
        public string FtpUser { get; set; }
        public string FtpPassword { get; set; }
        public string FtpDirectoryIn { get; set; }
        public string FtpDirectoryOut { get; set; }
        public string RKK_NbchResponcePath { get; set; }
        public string RKK_EiResponcePath { get; set; }
        public string RKK_EquifaxResponcePath { get; set; }
        public string ScoringEiResponcePath { get; set; }
        public string ScoringEquifaxResponcePath { get; set; }
    }
}